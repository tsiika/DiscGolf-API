/*
*   models/Round
*
*   Collection: rounds
*   
*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//const Course = require('./course');

const roundSchema = new Schema({

    courseId: { type: Schema.Types.ObjectId, required: true },
    course: { type: Schema.Types.ObjectId, ref: 'course' },
    
    players: [
        { type: Schema.Types.ObjectId, required: true }
    ],

    scores: {},
    /* At the moment, scores-object should be formed like:
    scores: {
        'playerId': {
            'fairwayOrder': {
                throwCount: 2, ob: false
            },
            'fairwayOrder': ...,
            totalThrowCount: xx,
            playerName: 'username'
        },
        'playerId': ...
    }
    */

    created: { type: Date, default: Date.now, required: true },
    updated: { type: Date, default: Date.now, required: true }
});

const Round = mongoose.model('round', roundSchema);

module.exports = Round;
