/*
*   models/Course
*
*   Collection: courses
*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fairwaySchema = new Schema({

    _id:    false,
    id:     false,
    order:  {type: Number, required: true},
    length: {type: Number, required: true, min: 1, max: 9999},
    par:    {type: Number, required: true, min: 1, max: 9999}
});

const courseSchema = new Schema({
    
    name:           {type: String, required: true, minlength: 4, maxlength: 999},
    description:    {type: String, required: true, minlength: 4, maxlength: 999},
    fairways:       [ fairwaySchema ],
    created:        { type: Date, default: Date.now, required: true },
    updated:        { type: Date, default: Date.now, required: true }
});

const Course = mongoose.model('course', courseSchema);

module.exports = Course;