const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({

    username:   {type: String, required: true, unique: true, minlength: 3, maxlength: 99},
    email:      {type: String, required: true, unique: true, minlength: 5, maxlength: 99},
    password:   {type: String, required: true, minlength: 4, maxlength: 99},
    roles:      [String], // List of allowed user roles. Used for request authorization.
    created:    { type: Date, default: Date.now, required: true },
    updated:    { type: Date, default: Date.now, required: true }

}); //, {collection: 'user'});
// Note: By default Mongoose pluralizes collection name from the model name,
// but this could be overwritten by defining the collection name in the Schemas option parameters.

// Check if the user has a require role
schema.hasRole = function (requiredRole) {
    for (const role in this.roles) if (role === requiredRole) return true;
    return false;
}

const User = mongoose.model('user', schema);
module.exports = User;
