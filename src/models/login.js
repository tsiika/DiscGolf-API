const mongoose = require('mongoose');
const User = require('./user');
const Schema = mongoose.Schema;


const schema = new Schema({
    userId: Schema.Types.ObjectId,
    token: String,
    loginDate: { type: Date, default: Date.now }
});

schema.statics.findUserByToken = (token) => {
    Login.findOne({ token: token }, (err, login) => {
        if (err) throw err;
        return User.findById(token.userId);
    });
};

schema.statics.deleteByToken = (token) => {
    Login.deleteOne({ "token": token }, (err) => {
        if (err) throw err;
    });
};

schema.statics.requireRole = (role) => {
    return (req, res, next) => {
        /*
        const token = req.header.token;
        const user = Login.findUserByToken(token);

        return next(); // FIXME: Allows all request for now.

        if (user && user.hasRole(role))
            next();
        else
            res.status(403).json({ "message": "Unauthorized" });
        */
        next();
    };
}

const Login = mongoose.model('login', schema);
module.exports = Login;
