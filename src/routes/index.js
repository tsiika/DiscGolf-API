const express = require('express');
const router = express.Router();

router.get('/', function(req, res, next) {

    res.send('Response from index route');
});

// Note that we export only the router-object, so this module is externally used as router-object,
// not as generic 'class-object'
module.exports = router;