const express = require('express');
const router = express.Router();
const User = require('../models/user');
const requireJson = require('../utils/utils').requireJson;
const jwt = require('jsonwebtoken');
const config = require('config');
const bcrypt = require('bcrypt');

// Set Access-Control-Allow-Origin for all routes
router.use('*', (req, res, next) => {
    res.set('Access-Control-Allow-Origin', req.headers.origin);
    next();
});

// POST authenticate user
// TODO: Do separate Client authentication
router.post('/authenticate/user', requireJson, (req, res, next) => {

    // Check for username and password
    let {username, password} = req.body;
    if(!username || username === '' || !password || password === '') {
        res.status(400).json({error: 'Invalid authentication credentials'});
        return false;
    }
    
    User.findOne({'username': username}, (error, user) => {

        if(error) {
            res.status(400).json({error: error._message || error.message});
        } else if(user && user._id) {

            // Check password
            bcrypt.compare(password, user.password, function(error, same) {
                if(error) {
                    res.status(500).json({error: error});
                } else if(same) {
                    
                    const tokenPayload = { userId: user._id, username: user.username, role: user.roles[0] };
                    const tokenSecret  = config.get('auth.accessTokenSecret');
                    const tokenOptions = { expiresIn: config.get('auth.accessTokenExpiry') };

                    // TODO: Move token signing to Utils-module
                    jwt.sign(tokenPayload, tokenSecret, tokenOptions, function(error, token) {
                        if(error) {
                            res.status(500).json({error: error});
                        } else if(token) {
                            res.status(200).json({ userId: user._id, accessToken: token });
                        } else {
                            res.status(500).json({error: 'Error on signing access token'});
                        }    
                    });

                } else {
                    res.status(401).json({error: 'Invalid authentication credentials'});        
                }
            });
            
        } else {
            res.status(401).json({error: 'Invalid authentication credentials'});
        }
    });
});


router.post('/register/user', requireJson, (req, res, next) => {
    
    // Check for params
    let { username, email, password } = req.body;
    if(!username || username === '' || !email || email === '' || !password || password === '') {
        res.status(400).json({error: 'Invalid user registration parameters'});
        return false;
    }

    // Check for existing username or email
    User.find({ $or: [ {'username': username}, {'email': email} ] }, (error, users) => {
        if(users.length > 0) {
            res.status(400).json({error: 'Username or email already in use'});
            return;
        }

        // Create new User-object
        var user = new User({
            username: username,
            email: email,
            password: '', // FIXME: Crypt the password
            roles: ['player'] // Default role for new users?
        });

        // Encrypt password
        // TODO: Move encryption to Utils-module...
        bcrypt.hash(password, 10, function(error, hash) {
            
            if(error) {
                res.status(500).json({error: error});
                return false;
            }

            user.password = hash;
            
            // Try to save user into database. Note that Mongoose-validation is not done until this point.
            user.save(function (err, user) {
                // FIXME: Returning error or handling empty result...
                if(err) {
                    res.status(400).json( { error: err._message || err.message } );
                } else {
                    // TODO: Move Token singning to Utils-module
                    let token = jwt.sign(
                        { userId: user._id, username: user.username, role: user.roles[0] }, 
                        config.get('auth.accessTokenSecret'),
                        { expiresIn: config.get('auth.accessTokenExpiry') }
                    );

                    res.status(201).json({ userId: user._id, accessToken: token });
                }
            });
        });
    });

});


module.exports = router;