const express = require('express');
const router = express.Router();

const Course = require('../models/course');

// Set Access-Control-Allow-Origin for all routes
router.use('*', (req, res, next) => {
    res.set('Access-Control-Allow-Origin', req.headers.origin);
    next();
});

// GET Courses
router.get('/', function(req, res, next) {

    Course.find(function(err, courses) {
        if(err) {
            console.error(err);
            res.json( {error: err} );
        }

        res.set('Access-Control-Allow-Origin', '*');
        res.json(courses);
    });
    
});

// GET Course by id
router.get('/:courseId', function(req, res, next) {

    let id = req.params.courseId;

    Course.findById(id, function(err, course) {
        
        if(err) {
            console.error(err);
            res.status(404).json( {error: err} );
        } else {
            res.set('Access-Control-Allow-Origin', '*');
            res.json(course);
        }
    });
    
});

// POST Course
router.post('/', function(req, res, next) {

    res.set('Access-Control-Allow-Origin', req.headers.origin);

    // Create new Course-object
    let course = new Course(req.body);
    
    course.save(function(err, course) {

        if(err) {
            console.error(err);
            res.status(500).json({error: err});
        } else {
            res.json(course);
        }
    });
});

// PUT Course
router.put('/', function(req, res, next) {

    res.set('Access-Control-Allow-Origin', req.headers.origin);

    let id = req.body._id;
    
    if(!id || id === null) {
        res.status(400).json({error: 'No id provided'});
    }

    // Try to find existing object
    Course.findById(id, function(err, course) {
        
        if(err) {
            res.status(404).json({ error: 'Course not found', requestBody: req.body });
        } else {

            // Update data
            // TODO: Do this more carefully
            course.name = req.body.name;
            course.description = req.body.description;
            course.fairways = req.body.fairways;
            
            // Try to save object to database
            course.save(function(err, updatedCourse) {
                
                if(err) {
                    res.status(500).json({ error: 'Error saving course', requestBody: req.body });
                } else {
                    // Return 200 with updated object
                    res.json(updatedCourse);
                }
            });
        }        
    });
});

module.exports = router;