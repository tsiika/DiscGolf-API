const express = require('express');
const router = express.Router();

const Round = require('../models/round');

// Set Access-Control-Allow-Origin for all routes
router.use('*', (req, res, next) => {
    res.set('Access-Control-Allow-Origin', req.headers.origin);
    next();
});

// GET rounds
router.get('/', function(req, res, next) {

    Round.find(function(err, rounds){
        if(err) {
            console.error(err);
            res.json( {error: err} );
        }

        res.set('Access-Control-Allow-Origin', '*');
        res.json(rounds);
    });    
});

// GET round by id
router.get('/:roundId', (req, res, next) => {

    const id = req.params.roundId;

    Round.findById(id, function(err, round) {

        if(err) {
            res.status(400).json({error: err});
        } else if(round) {
            res.status(200).json(round);
        } else {
            res.status(404).json({error: 'No round found'});
        }
        
    });
});

// POST round
router.post('/', function(req, res, next) {

    let round = new Round();
    
    let courseId = req.body.courseId || req.body.course._id;

    round.courseId = courseId;
    round.course = courseId;
    round.players = req.body.players;
    round.scores = req.body.scores;
    
    round.save(function(err, round) {

        if(err) {
            console.error(err);
            res.status(500).json({error: err});
        }

        res.set('Access-Control-Allow-Origin', req.headers.origin);
        res.json(round);
    });
});

// PUT round
router.put('/', function(req, res, next) {

    res.set('Access-Control-Allow-Origin', req.headers.origin);

    let id = req.body._id;
    
    if(!id || id === null) {
        res.status(400).json({error: 'No id provided'});
    }

    // Try to find existing object
    Round.findById(id, function(err, round) {
        
        if(err) {
            res.status(404).json({ error: 'Round not found', requestBody: req.body });
        }

        // Update score data
        round.scores = req.body.scores;
        
        // Try to save object to database
        round.save(function(err, updatedRound) {
            
            if(err) {
                res.status(500).json({ error: 'Error saving round', requestBody: req.body });
            }

            // Return 200 with updated object
            res.json(updatedRound);
        });
    });
});

module.exports = router;