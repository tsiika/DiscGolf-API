const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Round = require('../models/round');
const requireRole = require('../models/login').requireRole;
const requireJson = require('../utils/utils').requireJson;

// Set Access-Control-Allow-Origin for all routes
router.use('*', (req, res, next) => {
    res.set('Access-Control-Allow-Origin', req.headers.origin);
    next();
});

// GET users
router.get('/', requireRole('player'), (req, res, next) => {
    console.log('GET /users');

    User.find(function (err, users) {
        
        if (err) {
            console.error(err);
            res.status(404).json( { error: err._message || err.message } );
        } else {
            res.status(200).json(users);
        }        
    });
});

// GET user by id
router.get('/:userId', requireRole('player'), (req, res, next) => {
    console.log('GET /users/:userId');
    console.log('ID: ' + req.params.userId);
    const id = req.params.userId;
    console.log('ID AGAIN: ' + id);

    User.findById(id, function(err, user) {
        console.log("FINDING BY ID...");
        if(err) {
            console.log('...ERROR: ');
            console.log(err);
            res.status(400).json({error: err});
        } else {
            console.log('...NO ERROR');
            if(user) {
                res.status(200).json(user);
            } else {
                res.status(404).json(user);
            }
        }
    });
});

// POST user
router.post('/', requireJson, requireRole('player'), (req, res, next) => {
    console.log('POST /users');
    // TODO: Check password confirmation...

    // Create new User-object
    var user = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password, // FIXME: Crypt the password
        roles: ['player'] // Default role for new users?
    });

    // Try to save user into database. Note that Mongoose-validation is not done until this point.
    user.save(function (err, user) {
        // FIXME: Returning error or handling empty result...
        if(err) {
            console.error(err);
            res.status(400).json( { error: err._message || err.message } );
        } else {
            res.status(201).json( { id: user._id } );
        }
    });
});

// GET user's rounds by user id
router.get('/:userId/rounds', requireRole('player'), (req, res, next) => {
    
    const userId = req.params.userId;
        
    // FIXME: Returning error or handling empty result...
    // FIXME: What if Course is not found anymore (removed etc...)?
    Round.find({players: { $elemMatch:{ $eq: userId } }}).populate('course').exec((error, rounds) => {
        
        if(error) {
            console.error(error);
            res.status(404).json( {error: error._message || error.message} );
        } else {
            res.status(200).json(rounds);
        }
    });
    
});

// Note that we export only the router-object, so this module is externally used as router-object,
// not as generic 'class-object'
module.exports = router;
