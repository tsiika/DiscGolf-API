
const utils = {};

// Checks that request Content-Type is 'application/json'
utils.requireJson = (req, res, next) => {

    // Note that req.is() allways returns null, if there is no body in the request 
    if(!req.is('application/json')) {
        res.status(415).json( { error: 'Invalid Content-Type'} );
    } else {
        next();
    }    
};

module.exports = utils;