console.log("NODE_ENV: " + process.env.NODE_ENV);
console.log("NODE_APP_INSTANCE: " + process.env.NODE_APP_INSTANCE);
console.log("PORT: " + process.env.PORT);

const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const bodyParser = require('body-parser');

// Configuration
// Using https://www.npmjs.com/package/config for configuration.
// TODO: Configuration should be implemented for different environments
// TODO: There's probably some way to set Config to read straight from environment variables
const mongodbHost = process.env.MONGODB_HOST || config.get('dbConfig.MONGODB_HOST');
const mongodbPort = process.env.MONGODB_PORT || config.get('dbConfig.MONGODB_PORT');
const mongodbName = process.env.MONGODB_NAME || config.get('dbConfig.MONGODB_NAME');
const mongodbUser = process.env.MONGODB_USER || config.get('dbConfig.MONGODB_USER');
const mongodbPassword = process.env.MONGODB_PASSWORD || config.get('dbConfig.MONGODB_PASSWORD');

// Build uri for Mongodb connection
let mongodbURI = '';

// FIXME: Enable user credentials for localhost
if(mongodbHost === 'localhost') {
    mongodbURI = 'mongodb://' + mongodbHost + '/' + mongodbName;
} else {
    mongodbURI = 'mongodb://' + mongodbUser + ':' + mongodbPassword + '@' + mongodbHost + ':' + mongodbPort + '/' + mongodbName;
}

// Init Express configuration
const expressPort = process.env.PORT || config.get('expressConfig.port');
const expressHost = config.get('expressConfig.host');

// Init Mongoose database connection
mongoose.connect(mongodbURI).then(() => {
    console.log('Database connection opened @' + mongodbHost + '/' + mongodbName);
},(error) => {
    throw new Error(error);
}).catch((error) => {
    console.error(error);
    process.exit();
});

// Init Express
const app = express();

// Init body-parser to parse request body-data
// NOTE: This has to be done before registering routes
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// OPTIONS
//
// Note: OPTION-request are made from client, according to CORS-standard as 'preflighted' requests, when the actual request is, for
// example, a POST-request with content-type:'application/json' (other scenarios as well).
// Preflighted requested must be handeled by responding at least with allowed origins, methods and headers,
// to client to able to make the actual request.
// More about CORS: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
app.options('*', function (req, res, next) {
    
    res.set('Access-Control-Allow-Origin', req.headers.origin);
    res.set('Access-Control-Allow-Methods', 'POST, PUT, DELETE');
    res.set('Access-Control-Allow-Headers', 'Content-Type');

    res.send('OPTIONS response');
});

// Define endpoints and register routes
app.use('/', require('./routes/index'));

app.use('/api/v0/auth', require('./routes/auth'));

app.use('/api/v0/users', require('./routes/users'));
app.use('/api/v0/courses', require('./routes/courses'));
app.use('/api/v0/rounds', require('./routes/rounds'));

// Catch 404
// TODO: Implement stronger error handling.
app.use((req, res, next) => {
    res.status(404).json({ "message": "Resource not found" });
});

app.use((err, req, res, next) => {
    //console.error(err.stack);
    console.log(err);
    res.status(500).json({ "message": err.message });
});

// Start the Express server. Note that host is not defined, so it defaults to 'localhost'
app.listen(expressPort, (error) => {
    
    if(error) {
        console.error(error);
        process.exit();
    }

    console.log('App listening on localhost:' + expressPort + ', env:' + app.get('env'));
});
